
const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for CREATING A COURSE

router.post("/", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send(false);
	}
	else
	{
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));	
	}
});

// Route for RETRIEVING ALL THE COURSE

router.get("/all", (req,res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for RETRIEVING ALL THE ACTIVE COURSES

router.get("/active", (req,res) => {

	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController));
});

// Route for RETRIEVING SPECIFIC COURSE

router.get("/:courseId", (req,res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for UPDATING A COURSE

router.put("/:courseId", auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for ARCHIVING A COURSE

router.patch("/:courseId/archive", auth.verify, (req,res) => {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;