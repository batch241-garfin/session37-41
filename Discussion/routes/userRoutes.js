
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for CHECKING IF THE USER'S EMAIL ALREADY EXISTS IN THE DATABASE

router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for USER REGISTRATION

router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for USER AUTHENTICATION

router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for GETTING THE USER BY ITS ID

router.post("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.detailsOfUser({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route for ENROLLING USER TO A COURSE

router.post("/enroll", auth.verify, (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;