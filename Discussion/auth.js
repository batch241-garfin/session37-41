
const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";


// Creation of TOKEN
module.exports.createAccessToken = (user) => {

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// jwt.sign(data/payload, secretkey, option);
	return jwt.sign(data, secret, {});
}

// Token Verification

module.exports.verify = (req, res, nexts) => {

	let token = req.headers.authorization;

	if (typeof token !== "undefined") {

		//console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return res.send({auth: failed});
			}
			else
			{
				nexts();
			}
		});
	}
	else
	{
		return res.send({auth: "failed"});
	}
}

// Token Decryption

module.exports.decode = (token) => {

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return null;
			}
			else
			{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else
	{
		return null;
	}
}