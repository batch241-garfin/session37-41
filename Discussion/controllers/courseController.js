
const Course = require("../models/Course");
const User = require("../models/User");
const auth = require("../auth");


// Retrieving ADD COURSE

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course
	({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if (error) {

			return false;
		}
		else
		{
			return true;
		}
	})

	// let message = Promise.resolve("User must be ADMIN");
	// return message.then((value) => { return {value};
	// })
};

// Retrieving ALL COURSES

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieving ALL ACTIVE COURSES

module.exports.getAllActiveCourses = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieving ALL SPECIFIC COURSE

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Retrieving ALL SPECIFIC COURSE

module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {
		name:reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// findBiIdAndUpdate(doucmentId, updateToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {

		if (error) {
			return false;
		}
		else
		{
			return true;
		};
	});
};

// Archiving A Course

module.exports.archiveCourse = (reqParams, reqBody) => {

	let patchCourse = {
		isActive: false
	};

	// findBiIdAndUpdate(doucmentId, updateToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, patchCourse).then((course, error) => {

		if (error) {
			return false;
		}
		else
		{
			return true;
		};
	});
};